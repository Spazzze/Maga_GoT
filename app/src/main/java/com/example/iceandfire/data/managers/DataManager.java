package com.example.iceandfire.data.managers;

import android.content.Context;

import com.example.iceandfire.App;
import com.example.iceandfire.data.network.RestService;
import com.example.iceandfire.data.network.ServiceGenerator;
import com.example.iceandfire.data.network.restmodels.CharactersRes;
import com.example.iceandfire.data.network.restmodels.HousesRes;
import com.example.iceandfire.data.storage.models.Aliases;
import com.example.iceandfire.data.storage.models.Characters;
import com.example.iceandfire.data.storage.models.CharactersDao;
import com.example.iceandfire.data.storage.models.DaoSession;
import com.example.iceandfire.data.storage.models.Houses;
import com.example.iceandfire.data.storage.models.HousesDao;
import com.example.iceandfire.data.storage.models.Titles;

import org.greenrobot.greendao.async.AsyncSession;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class DataManager {
    private static DataManager INSTANCE = null;

    private Context mContext;
    private RestService mRestService;
    private DaoSession mDaoSession;
    private AsyncSession mAsyncDbSession;

    public DataManager() {
        mRestService = ServiceGenerator.createService(RestService.class);
        mContext = App.getContext();
        mDaoSession = App.getDaoSession();
        mAsyncDbSession = mDaoSession.startAsyncSession();
    }

    public static DataManager getInstance() {
        if (INSTANCE == null){
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public Context getContext() {
        return mContext;
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    //region ================= Network ==================

    public Call<HousesRes> loadHouse(int num) {
        return mRestService.getHouse(num);
    }

    public Call<List<CharactersRes>> loadCharacters(int page) {
        return mRestService.getCharacters(page, 50);
    }

    //endregion

    //region ================= Database ==================

    public void saveHousesInDb(Houses houses) {
        mAsyncDbSession.insertOrReplace(houses);
    }

    public void saveCharactersInDb(List<Characters> charactersList) {
        mAsyncDbSession.insertOrReplaceInTx(Characters.class, charactersList);
    }

    public void saveTitleInDb(List<Titles> titlesList) {
        mAsyncDbSession.insertOrReplaceInTx(Titles.class, titlesList);
    }

    public void saveAliasesInDb(List<Aliases> aliasesList) {
        mAsyncDbSession.insertOrReplaceInTx(Aliases.class, aliasesList);
    }

    public Characters getCharacterUniqueFromDb(String url) {
        Characters character = new Characters();

        try {
            character = mDaoSession.queryBuilder(Characters.class)
                    .where(CharactersDao.Properties.Url.eq(url))
                    .build()
                    .unique();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return character;
    }

    public List<Characters> getCharactersFromDb() {
        List<Characters> charactersList = new ArrayList<>();

        try {
            charactersList = mDaoSession.queryBuilder(Characters.class)
                    .orderAsc(CharactersDao.Properties.Name)
                    .build()
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return charactersList;
    }

    public List<Characters> getCharactersFromDbByQuery(String url) {
        List<Characters> charactersList = new ArrayList<>();

        try {
            charactersList = mDaoSession.queryBuilder(Characters.class)
                    .where(CharactersDao.Properties.Allegiance.eq(url))
                    .orderAsc(CharactersDao.Properties.Name)
                    .build()
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return charactersList;
    }

    public List<Houses> getHousesFromDb() {
        List<Houses> houses = new ArrayList<>();

        try {
            houses = mDaoSession.queryBuilder(Houses.class)
                    .orderAsc(HousesDao.Properties.Name)
                    .build()
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return houses;
    }

    //endregion

}
