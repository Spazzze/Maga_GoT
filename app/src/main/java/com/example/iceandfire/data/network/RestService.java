package com.example.iceandfire.data.network;

import com.example.iceandfire.data.network.restmodels.CharactersRes;
import com.example.iceandfire.data.network.restmodels.HousesRes;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestService {
    @GET("houses/{num}")
    Call<HousesRes> getHouse(@Path("num") int num);

    @GET("characters")
    Call<List<CharactersRes>> getCharacters(@Query("page") int page,
                                            @Query("pageSize") int pageSize);
}
