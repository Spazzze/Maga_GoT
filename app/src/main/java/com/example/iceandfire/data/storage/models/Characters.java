package com.example.iceandfire.data.storage.models;

import com.example.iceandfire.data.network.restmodels.CharactersRes;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Unique;

import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

@Entity(active = true, nameInDb = "CHARACTERS")
public class Characters {

    @Id
    private Long id;

    @NotNull
    @Unique
    private String name;

    @Unique
    private String url;

    private String words;

    private String born;

    @ToMany(joinProperties = {
            @JoinProperty(name = "url", referencedName = "parentUrl")
    })
    private List<Titles> titles;

    @ToMany(joinProperties = {
            @JoinProperty(name = "url", referencedName = "parentUrl")
    })
    private List<Aliases> aliases;

    private String father;

    private String mother;

    private String allegiance;

    public Characters(CharactersRes charactersRes) {
        this.name = charactersRes.name;
        this.url = charactersRes.url;
        this.born = charactersRes.born;
        this.father = charactersRes.father;
        this.mother = charactersRes.mother;
        if (!charactersRes.allegiances.isEmpty()) {
            this.allegiance = charactersRes.allegiances.get(0);
        }
    }

    /** Used to resolve relations */
@Generated(hash = 2040040024)
private transient DaoSession daoSession;

/** Used for active entity operations. */
@Generated(hash = 133066945)
private transient CharactersDao myDao;

@Generated(hash = 909921966)
public Characters(Long id, @NotNull String name, String url, String words, String born,
        String father, String mother, String allegiance) {
    this.id = id;
    this.name = name;
    this.url = url;
    this.words = words;
    this.born = born;
    this.father = father;
    this.mother = mother;
    this.allegiance = allegiance;
}

@Generated(hash = 1941317426)
public Characters() {
}

public Long getId() {
    return this.id;
}

public void setId(Long id) {
    this.id = id;
}

public String getName() {
    return this.name;
}

public void setName(String name) {
    this.name = name;
}

public String getUrl() {
    return this.url;
}

public void setUrl(String url) {
    this.url = url;
}

public String getWords() {
    return this.words;
}

public void setWords(String words) {
    this.words = words;
}

public String getBorn() {
    return this.born;
}

public void setBorn(String born) {
    this.born = born;
}

public String getFather() {
    return this.father;
}

public void setFather(String father) {
    this.father = father;
}

public String getMother() {
    return this.mother;
}

public void setMother(String mother) {
    this.mother = mother;
}

/**
 * To-many relationship, resolved on first access (and after reset).
 * Changes to to-many relations are not persisted, make changes to the target entity.
 */
@Generated(hash = 2045071683)
public List<Titles> getTitles() {
    if (titles == null) {
        final DaoSession daoSession = this.daoSession;
        if (daoSession == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        TitlesDao targetDao = daoSession.getTitlesDao();
        List<Titles> titlesNew = targetDao._queryCharacters_Titles(url);
        synchronized (this) {
            if (titles == null) {
                titles = titlesNew;
            }
        }
    }
    return titles;
}

/** Resets a to-many relationship, making the next get call to query for a fresh result. */
@Generated(hash = 1506933621)
public synchronized void resetTitles() {
    titles = null;
}

/**
 * To-many relationship, resolved on first access (and after reset).
 * Changes to to-many relations are not persisted, make changes to the target entity.
 */
@Generated(hash = 326132584)
public List<Aliases> getAliases() {
    if (aliases == null) {
        final DaoSession daoSession = this.daoSession;
        if (daoSession == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        AliasesDao targetDao = daoSession.getAliasesDao();
        List<Aliases> aliasesNew = targetDao._queryCharacters_Aliases(url);
        synchronized (this) {
            if (aliases == null) {
                aliases = aliasesNew;
            }
        }
    }
    return aliases;
}

/** Resets a to-many relationship, making the next get call to query for a fresh result. */
@Generated(hash = 731614754)
public synchronized void resetAliases() {
    aliases = null;
}

/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 128553479)
public void delete() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.delete(this);
}

/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 1942392019)
public void refresh() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.refresh(this);
}

/**
 * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
 * Entity must attached to an entity context.
 */
@Generated(hash = 713229351)
public void update() {
    if (myDao == null) {
        throw new DaoException("Entity is detached from DAO context");
    }
    myDao.update(this);
}

/** called by internal mechanisms, do not call yourself. */
@Generated(hash = 1742025657)
public void __setDaoSession(DaoSession daoSession) {
    this.daoSession = daoSession;
    myDao = daoSession != null ? daoSession.getCharactersDao() : null;
}

public String getAllegiance() {
    return this.allegiance;
}

public void setAllegiance(String allegiance) {
    this.allegiance = allegiance;
}
}
