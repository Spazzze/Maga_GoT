package com.example.iceandfire.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.iceandfire.mvp.models.SplashScreenModel;
import com.example.iceandfire.mvp.views.ISplashScreenView;

public class SplashScreenPresenter implements ISplashScreenPresenter {

    private static SplashScreenPresenter ourInstance = new SplashScreenPresenter();
    private ISplashScreenView mSplashScreenView;
    private SplashScreenModel mModel;

    private SplashScreenPresenter() {
        mModel = new SplashScreenModel();
    }

    public static SplashScreenPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(ISplashScreenView view) {
        mSplashScreenView = view;
    }

    @Override
    public void dropView() {
        mSplashScreenView = null;
    }

    @Override
    public void initView() {
        if (getView() != null) {
            if (mModel.getHousesFromDb().isEmpty() && mModel.getCharactersFromDb().isEmpty()) {
                mModel.loadHouses();
                mModel.loadCharacters();
            } else {
                getView().goToNextScreen();
            }
        }
    }

    @Nullable
    @Override
    public ISplashScreenView getView() {
        return mSplashScreenView;
    }

    @Override
    public void loadEnd() {
        if (getView() != null) {
            getView().goToNextScreen();
        }
    }

    @Override
    public void showMessage(String message) {
        if (getView() != null) {
            getView().showMessage(message);
        }
    }
}
