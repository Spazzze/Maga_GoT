package com.example.iceandfire.mvp.models;

import com.example.iceandfire.data.managers.DataManager;
import com.example.iceandfire.data.storage.models.Characters;

import java.util.List;

public class CharacterListModel {

    private DataManager mDataManager;

    public CharacterListModel() {
        mDataManager = DataManager.getInstance();
    }

    public List<Characters> getCharactersFromDbByQuery(String url) {
        return mDataManager.getCharactersFromDbByQuery(url);
    }
}
