package com.example.iceandfire.mvp.models;

import com.example.iceandfire.data.managers.DataManager;
import com.example.iceandfire.data.network.restmodels.CharactersRes;
import com.example.iceandfire.data.network.restmodels.HousesRes;
import com.example.iceandfire.data.storage.models.Aliases;
import com.example.iceandfire.data.storage.models.Characters;
import com.example.iceandfire.data.storage.models.Houses;
import com.example.iceandfire.data.storage.models.Titles;
import com.example.iceandfire.mvp.presenters.SplashScreenPresenter;
import com.example.iceandfire.utils.ConstantManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreenModel {
    final int[] housesNum = {
            ConstantManager.LANNISTER,
            ConstantManager.STARK,
            ConstantManager.TARGARYEN
    };

    private DataManager mDataManager;

    public SplashScreenModel() {
        mDataManager = DataManager.getInstance();
    }



    public void loadHouses() {
        for (int i : housesNum) {
            Call<HousesRes> call = mDataManager.loadHouse(i);
            call.enqueue(new Callback<HousesRes>() {
                @Override
                public void onResponse(Call<HousesRes> call, Response<HousesRes> response) {
                    try {
                        if (response.code() == 200) {

                            mDataManager.saveHousesInDb(new Houses(response.body()));

                        } else {
                            SplashScreenPresenter.getInstance().showMessage("Сервер не может обработать запрос");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<HousesRes> call, Throwable t) {
                    t.printStackTrace();
                    SplashScreenPresenter.getInstance().showMessage("Что-то пошло не так");
                }
            });
        }
    }

    public void loadCharacters() {
        for (int i = 1; i <= 43; i++) {
            Call<List<CharactersRes>> call = mDataManager.loadCharacters(i);
            final int iter = i;
            call.enqueue(new Callback<List<CharactersRes>>() {
                @Override
                public void onResponse(Call<List<CharactersRes>> call, Response<List<CharactersRes>> response) {
                    try {
                        if (response.code() == 200) {

                            List<Characters> allCharacters = new ArrayList<>();
                            List<Aliases> allAliases = new ArrayList<>();
                            List<Titles> allTitles = new ArrayList<>();

                            for (CharactersRes res : response.body()) {
                                allCharacters.add(new Characters(res));
                                if (res.aliases != null) {
                                    for (String aliase : res.aliases) {
                                        allAliases.add(new Aliases(aliase, res.url));
                                    }
                                }
                                if (res.titles != null) {
                                    for (String title : res.titles) {
                                        allTitles.add(new Titles(title, res.url));
                                    }
                                }
                            }
                            mDataManager.saveCharactersInDb(allCharacters);
                            mDataManager.saveTitleInDb(allTitles);
                            mDataManager.saveAliasesInDb(allAliases);

                            if (iter == 43) {
                                SplashScreenPresenter.getInstance().loadEnd();
                            }

                        } else {
                            SplashScreenPresenter.getInstance().showMessage("Сервер не может обработать запрос");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<List<CharactersRes>> call, Throwable t) {
                    t.printStackTrace();
                    SplashScreenPresenter.getInstance().showMessage("Что-то пошло не так");
                }
            });
        }
    }

    public List<Houses> getHousesFromDb() {
        return mDataManager.getHousesFromDb();
    }

    public List<Characters> getCharactersFromDb() {
        return mDataManager.getCharactersFromDb();
    }
}
