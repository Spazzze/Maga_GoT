package com.example.iceandfire.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.iceandfire.mvp.views.ICharacterView;

public interface ICharacterPresenter {

    void takeView(ICharacterView view);
    void dropView();
    void initView();

    @Nullable
    ICharacterView getView();

    void getData(String url);

}
