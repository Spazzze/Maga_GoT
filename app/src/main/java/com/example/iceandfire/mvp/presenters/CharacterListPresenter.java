package com.example.iceandfire.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.iceandfire.ui.adapters.CharactersAdapter;
import com.example.iceandfire.mvp.models.CharacterListModel;
import com.example.iceandfire.mvp.views.ICharacterListView;

public class CharacterListPresenter implements ICharacterListPresenter {

    private static CharacterListPresenter ourInstance = new CharacterListPresenter();
    private ICharacterListView mCharacterListView;
    private CharacterListModel mModel;

    private CharacterListPresenter() {
        mModel = new CharacterListModel();
    }

    public static CharacterListPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(ICharacterListView view) {
        mCharacterListView = view;
    }

    @Override
    public void dropView() {
        mCharacterListView = null;
    }

    @Override
    public void initView(String url) {
        if (getView() != null) {
            CharactersAdapter adapter = new CharactersAdapter(mModel.getCharactersFromDbByQuery(url));
            getView().setAdapter(adapter);
        }
    }

    @Nullable
    @Override
    public ICharacterListView getView() {
        return mCharacterListView;
    }
}
