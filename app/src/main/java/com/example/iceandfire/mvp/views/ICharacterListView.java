package com.example.iceandfire.mvp.views;

import com.example.iceandfire.ui.adapters.CharactersAdapter;
import com.example.iceandfire.mvp.presenters.ICharacterListPresenter;

public interface ICharacterListView {

    void showMessage(String message);
    void showError(Throwable e);
    void showLoad();
    void hideLoad();
    ICharacterListPresenter getPresenter();

    void setAdapter(CharactersAdapter charactersAdapter);

}
