package com.example.iceandfire.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.iceandfire.mvp.models.CharacterModel;
import com.example.iceandfire.mvp.views.ICharacterView;

public class CharacterPresenter implements ICharacterPresenter {

    private static CharacterPresenter ourInstance = new CharacterPresenter();
    private ICharacterView mCharacterView;
    private CharacterModel mCharacterModel;

    private CharacterPresenter() {
        mCharacterModel = new CharacterModel();
    }

    public static CharacterPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void takeView(ICharacterView view) {
        mCharacterView = view;
    }

    @Override
    public void dropView() {
        mCharacterView = null;
    }

    @Override
    public void initView() {
        if (getView() != null) {
            // TODO: 22.10.2016
        }
    }

    @Nullable
    @Override
    public ICharacterView getView() {
        return mCharacterView;
    }

    @Override
    public void getData(String url) {
        if (getView() != null) {
            getView().showInfo(mCharacterModel.getCharacterUniqueFromDb(url), mCharacterModel.getHousesFromDb());
        }
    }
}
