package com.example.iceandfire.mvp.models;

import com.example.iceandfire.data.managers.DataManager;
import com.example.iceandfire.data.storage.models.Characters;
import com.example.iceandfire.data.storage.models.Houses;

import java.util.List;

public class CharacterModel {

    private DataManager mDataManager;

    public CharacterModel() {
        mDataManager = DataManager.getInstance();
    }

    public Characters getCharacterUniqueFromDb(String url) {
        return mDataManager.getCharacterUniqueFromDb(url);
    }

    public List<Houses> getHousesFromDb() {
        return mDataManager.getHousesFromDb();
    }

}
