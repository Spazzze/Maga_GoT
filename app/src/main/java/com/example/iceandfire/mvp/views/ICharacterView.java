package com.example.iceandfire.mvp.views;

import com.example.iceandfire.data.storage.models.Characters;
import com.example.iceandfire.data.storage.models.Houses;
import com.example.iceandfire.mvp.presenters.ICharacterPresenter;

import java.util.List;

public interface ICharacterView {

    void showMessage(String message);
    void showError(Throwable e);
    void showLoad();
    void hideLoad();
    ICharacterPresenter getPresenter();

    void showToolbar();
    void showInfo(Characters characters, List<Houses> housesList);

}
