package com.example.iceandfire.mvp.views;

import com.example.iceandfire.mvp.presenters.ISplashScreenPresenter;

public interface ISplashScreenView {

    void showMessage(String message);
    void showError(Throwable e);
    void showLoad();
    void hideLoad();
    ISplashScreenPresenter getPresenter();

    void goToNextScreen();

}
