package com.example.iceandfire.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.iceandfire.mvp.views.ICharacterListView;

public interface ICharacterListPresenter {

    void takeView(ICharacterListView view);
    void dropView();
    void initView(String url);

    @Nullable
    ICharacterListView getView();

}
