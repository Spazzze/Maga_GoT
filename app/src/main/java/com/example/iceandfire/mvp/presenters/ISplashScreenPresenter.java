package com.example.iceandfire.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.iceandfire.mvp.views.ISplashScreenView;

public interface ISplashScreenPresenter {

    void takeView(ISplashScreenView view);
    void dropView();
    void initView();

    @Nullable
    ISplashScreenView getView();

    void loadEnd();
    void showMessage(String message);

}
