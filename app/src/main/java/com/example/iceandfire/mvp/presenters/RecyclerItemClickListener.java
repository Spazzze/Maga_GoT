package com.example.iceandfire.mvp.presenters;

import android.view.View;

import com.example.iceandfire.data.storage.models.Characters;

import java.util.List;

public interface RecyclerItemClickListener {
    void onItemClickListener(View view, int position, List<Characters> charactersList);
}
