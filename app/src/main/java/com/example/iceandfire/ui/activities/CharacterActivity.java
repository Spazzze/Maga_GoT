package com.example.iceandfire.ui.activities;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.iceandfire.BuildConfig;
import com.example.iceandfire.R;
import com.example.iceandfire.data.managers.DataManager;
import com.example.iceandfire.data.storage.models.Aliases;
import com.example.iceandfire.data.storage.models.Characters;
import com.example.iceandfire.data.storage.models.Houses;
import com.example.iceandfire.data.storage.models.Titles;
import com.example.iceandfire.mvp.presenters.CharacterPresenter;
import com.example.iceandfire.mvp.presenters.ICharacterPresenter;
import com.example.iceandfire.mvp.views.ICharacterView;
import com.example.iceandfire.utils.ConstantManager;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CharacterActivity extends AppCompatActivity implements ICharacterView {

    private static final String TAG = "CharacterActivity";

    private CharacterPresenter mPresenter = CharacterPresenter.getInstance();

    @BindView(R.id.coordinator_character)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.toolbar_character)
    Toolbar mToolbar;
    @BindView(R.id.character_image)
    ImageView mCharacterImage;
    @BindView(R.id.character_word)
    TextView mCharacterWord;
    @BindView(R.id.character_born)
    TextView mCharacterBorn;
    @BindView(R.id.character_titles)
    TextView mCharacterTitles;
    @BindView(R.id.character_aliases)
    TextView mCharacterAliases;
    @BindView(R.id.character_father)
    TextView mCharacterFather;
    @BindView(R.id.character_mother)
    TextView mCharacterMother;

    private List<Houses> mHousesList;

    private DataManager mDataManager;
    private Characters mCharacter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);

        mDataManager = DataManager.getInstance();

        ButterKnife.bind(this);
        mPresenter.takeView(this);
        mPresenter.initView();

        mPresenter.getData(getIntent().getStringExtra(ConstantManager.URL_KEY));
        showToolbar();
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage("Что-то пошло не так, попробуйте позже");
        }
    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    @Override
    public ICharacterPresenter getPresenter() {
        return null;
    }

    @Override
    public void showToolbar() {
        mToolbar.setTitle(mCharacter.getName());
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null){
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void showInfo(Characters characters, List<Houses> housesList) {
        mCharacter = characters;
        mHousesList = housesList;

        if (mCharacter.getAllegiance().equals("http://anapioficeandfire.com/api/houses/" + ConstantManager.LANNISTER)) {
            Picasso.with(this)
                    .load(R.drawable.lannister)
                    .fit()
                    .into(mCharacterImage);
        } else if (mCharacter.getAllegiance().equals("http://anapioficeandfire.com/api/houses/" + ConstantManager.STARK)) {
            Picasso.with(this)
                    .load(R.drawable.stark)
                    .fit()
                    .into(mCharacterImage);
        } else {
            Picasso.with(this)
                    .load(R.drawable.targarien)
                    .fit()
                    .into(mCharacterImage);
        }


        StringBuilder titlesBuilder = new StringBuilder();
        for (Titles titles : mCharacter.getTitles()) {
            titlesBuilder.append(titles.getTitles());
            titlesBuilder.append("\n");
        }
        mCharacterTitles.setText(titlesBuilder);

        StringBuilder aliasesBuilder = new StringBuilder();
        for (Aliases aliases : mCharacter.getAliases()) {
            aliasesBuilder.append(aliases.getAliases());
            aliasesBuilder.append("\n");
        }
        mCharacterAliases.setText(aliasesBuilder);

        mCharacterBorn.setText(mCharacter.getBorn());
        try {
            mCharacterFather.setText(mDataManager.getCharacterUniqueFromDb(mCharacter.getFather()).getName());
            mCharacterMother.setText(mDataManager.getCharacterUniqueFromDb(mCharacter.getMother()).getName());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        for (Houses houses : mHousesList) {
            if (mCharacter.getAllegiance().equals(houses.getUrl())) {
                mCharacterWord.setText(houses.getWords());
            }
        }
    }
}
