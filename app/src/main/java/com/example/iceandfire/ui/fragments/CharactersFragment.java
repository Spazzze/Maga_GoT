package com.example.iceandfire.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.iceandfire.R;
import com.example.iceandfire.data.storage.models.Characters;
import com.example.iceandfire.ui.activities.CharacterActivity;
import com.example.iceandfire.ui.adapters.CharactersAdapter;
import com.example.iceandfire.mvp.presenters.CharacterListPresenter;
import com.example.iceandfire.mvp.presenters.ICharacterListPresenter;
import com.example.iceandfire.mvp.presenters.RecyclerItemClickListener;
import com.example.iceandfire.mvp.views.ICharacterListView;
import com.example.iceandfire.utils.ConstantManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CharactersFragment extends Fragment implements ICharacterListView, RecyclerItemClickListener {

    private CharacterListPresenter mPresenter = CharacterListPresenter.getInstance();

    @BindView(R.id.recycler_list)
    RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.character_content, container, false);

        ButterKnife.bind(this, view);

        mPresenter.takeView(this);
        mPresenter.initView(getArguments().getString(ConstantManager.ALLEGIANCE_KEY));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);

        return view;
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showError(Throwable e) {

    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    @Override
    public ICharacterListPresenter getPresenter() {
        return null;
    }


    @Override
    public void setAdapter(CharactersAdapter charactersAdapter) {
        mRecyclerView.setAdapter(charactersAdapter);
        charactersAdapter.setRecyclerItemClickListener(this);
    }

    @Override
    public void onItemClickListener(View view, int position, List<Characters> charactersList) {
        switch (view.getId()) {
            case R.id.item_characters_container:
                Intent intent = new Intent(getActivity(), CharacterActivity.class);
                intent.putExtra(ConstantManager.URL_KEY, charactersList.get(position).getUrl());
                intent.putExtra(ConstantManager.ALLEGIANCE_KEY, charactersList.get(position).getAllegiance());
                startActivity(intent);
                break;
        }
    }
}
