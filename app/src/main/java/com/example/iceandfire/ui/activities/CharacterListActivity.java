package com.example.iceandfire.ui.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.iceandfire.R;
import com.example.iceandfire.data.managers.DataManager;
import com.example.iceandfire.ui.adapters.PagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CharacterListActivity extends BaseActivity {

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;
    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.pager)
    ViewPager mViewPager;

    DataManager mDataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_list);

        mDataManager = DataManager.getInstance();

        ButterKnife.bind(this);

        setupTabs();
        setupDrawer();
        setupToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupTabs() {
        Menu menu = mNavigationView.getMenu();
        for (int i = 0; i < 3; i++) {
            menu.add(R.id.drawer_group, i, Menu.NONE, mDataManager.getHousesFromDb().get(i).getName());
        }
        mTabLayout.setTabGravity(TabLayout.MODE_SCROLLABLE);
        mTabLayout.setSelectedTabIndicatorColor(Color.WHITE);

        PagerAdapter pagerAdapter = new PagerAdapter
                (getSupportFragmentManager(), mDataManager.getHousesFromDb());
        mViewPager.setAdapter(pagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void setupDrawer() {
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                try {
                    mTabLayout.getTabAt(item.getItemId()).select();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }

    private void setupToolbar() {
        mToolbar.setTitle(R.string.app_name);
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
