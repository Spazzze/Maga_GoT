package com.example.iceandfire.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.iceandfire.R;
import com.example.iceandfire.data.storage.models.Characters;
import com.example.iceandfire.mvp.presenters.RecyclerItemClickListener;
import com.example.iceandfire.utils.ConstantManager;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CharactersAdapter extends RecyclerView.Adapter<CharactersAdapter.CharactersViewHolder> {

    private Context mContext;
    private List<Characters> mCharactersList;

    private RecyclerItemClickListener mRecyclerItemClickListener;

    public void setRecyclerItemClickListener(RecyclerItemClickListener recyclerItemClickListener) {
        this.mRecyclerItemClickListener = recyclerItemClickListener;
    }

    public CharactersAdapter(List<Characters> charactersList) {
        this.mCharactersList = charactersList;
    }

    @Override
    public CharactersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_characters_list, parent, false);
        return new CharactersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CharactersViewHolder holder, int position) {
        final Characters characters = mCharactersList.get(position);

        holder.mCharacterName.setText(characters.getName());

        StringBuilder titleBuilder = new StringBuilder();
        if (!characters.getTitles().isEmpty()) {
            for (int i = 0; i < characters.getTitles().size(); i++) {
                titleBuilder.append(characters.getTitles().get(i).getTitles());
                if (i < characters.getTitles().size() - 1) {
                    titleBuilder.append(" • ");
                }
            }
        } else {
            for (int i = 0; i < characters.getAliases().size(); i++) {
                titleBuilder.append(characters.getAliases().get(i).getAliases());
                if (i < characters.getAliases().size() - 1) {
                    titleBuilder.append(" • ");
                }
            }
        }
        holder.mCharacterTitles.setText(titleBuilder);

        if (characters.getAllegiance().equals("http://anapioficeandfire.com/api/houses/" + ConstantManager.LANNISTER)) {
            Picasso.with(mContext)
                    .load(R.drawable.lanister_icon)
                    .fit()
                    .into(holder.mImageItem);
        } else if (characters.getAllegiance().equals("http://anapioficeandfire.com/api/houses/" + ConstantManager.STARK)) {
            Picasso.with(mContext)
                    .load(R.drawable.stark_icon)
                    .fit()
                    .into(holder.mImageItem);
        } else {
            Picasso.with(mContext)
                    .load(R.drawable.targarien_icon)
                    .fit()
                    .into(holder.mImageItem);
        }
    }

    @Override
    public int getItemCount() {
        return mCharactersList.size();
    }


    public class CharactersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.image_item)
        ImageView mImageItem;
        @BindView(R.id.character_name_text)
        TextView mCharacterName;
        @BindView(R.id.character_titles_text)
        TextView mCharacterTitles;
        @BindView(R.id.item_characters_container)
        LinearLayout mItemContainer;

        public CharactersViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            mItemContainer.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (mRecyclerItemClickListener != null) {
                mRecyclerItemClickListener.onItemClickListener(v, getAdapterPosition(), mCharactersList);

            }
        }
    }
}
