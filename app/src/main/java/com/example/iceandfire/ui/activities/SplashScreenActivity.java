package com.example.iceandfire.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.iceandfire.R;
import com.example.iceandfire.mvp.presenters.ISplashScreenPresenter;
import com.example.iceandfire.mvp.presenters.SplashScreenPresenter;
import com.example.iceandfire.mvp.views.ISplashScreenView;

public class SplashScreenActivity extends BaseActivity implements ISplashScreenView{

    private SplashScreenPresenter mPresenter = SplashScreenPresenter.getInstance();

    private static final String TAG = "SplashScreen";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        showLoad();
        mPresenter.takeView(this);
        mPresenter.initView();


    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {

    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    @Override
    public ISplashScreenPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void goToNextScreen() {
        hideLoad();
        Intent intent = new Intent(this, CharacterListActivity.class);
        startActivity(intent);
        finish();
    }
}
