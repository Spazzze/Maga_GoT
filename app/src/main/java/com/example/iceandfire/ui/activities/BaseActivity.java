package com.example.iceandfire.ui.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.iceandfire.App;
import com.example.iceandfire.R;
import com.example.iceandfire.data.managers.DataManager;

public class BaseActivity extends AppCompatActivity {
    protected ProgressDialog mProgressDialog;

    public void showProgress(){
        if (mProgressDialog == null){
            mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_splash);
        } else {
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_splash);
        }
    }

    public void hideProgress(){
        if (mProgressDialog != null){
            if (mProgressDialog.isShowing()){
                mProgressDialog.dismiss();
            }
        }
    }

    public void showToast(String message) {
        Toast.makeText(DataManager.getInstance().getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
