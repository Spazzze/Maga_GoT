package com.example.iceandfire.ui.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.iceandfire.data.storage.models.Houses;
import com.example.iceandfire.ui.fragments.CharactersFragment;
import com.example.iceandfire.utils.ConstantManager;

import java.util.List;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private List<Houses> mHousesList;

    public PagerAdapter(FragmentManager fm, List<Houses> housesList) {
        super(fm);
        this.mHousesList = housesList;
    }

    @Override
    public Fragment getItem(int position) {

        Bundle args = new Bundle();
        args.putString(ConstantManager.ALLEGIANCE_KEY, mHousesList.get(position).getUrl());
        CharactersFragment charactersFragment = new CharactersFragment();
        charactersFragment.setArguments(args);
        return charactersFragment;
    }

    @Override
    public int getCount() {
        return mHousesList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mHousesList.get(position).getName();
    }
}
