package com.example.iceandfire.utils;

public interface ConstantManager {
    int LANNISTER = 229;
    int STARK = 362;
    int TARGARYEN = 378;

    String ALLEGIANCE_KEY = "ALLEGIANCE_KEY";
    String URL_KEY = "URL_KEY";
}
