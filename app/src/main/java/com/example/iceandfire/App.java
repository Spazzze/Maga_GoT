package com.example.iceandfire;

import android.app.Application;
import android.content.Context;

import com.example.iceandfire.data.storage.models.DaoMaster;
import com.example.iceandfire.data.storage.models.DaoSession;
import com.facebook.stetho.Stetho;

import org.greenrobot.greendao.async.AsyncSession;
import org.greenrobot.greendao.database.Database;

public class App extends Application {

    private static Context sContext;
    private static DaoSession sDaoSession;
    private static AsyncSession sAsyncSession;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = getApplicationContext();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "ice-and-fire-db");
        Database db = helper.getWritableDb();
        sDaoSession = new DaoMaster(db).newSession();
        sAsyncSession = new AsyncSession(sDaoSession);

        Stetho.initializeWithDefaults(this);
    }

    public static Context getContext() {
        return sContext;
    }

    public static DaoSession getDaoSession() {
        return sDaoSession;
    }

    public static AsyncSession getAsyncSession() {
        return sAsyncSession;
    }
}
